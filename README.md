First pass at making it easy to try out development environments using NixOS.

### Install

* install vagrant >= 1.65 (https://www.vagrantup.com/downloads.html)
* install vagrant-nixos plugin ("vagrant plugin install vagrant-nixos" or https://github.com/oxdi/vagrant-nixos)


### Quickstart

Try it out:

```
git clone https://gitlab.com/theerasmas/nixos-vagrant-quickstart.git
cd nixos-vagrant-quickstart
NIXOS_PROJECT='ghci' vagrant up
NIXOS_PROJECT='ghci' vagrant ssh
ghci
```

### Configure

To customize how the VM is created, modify the ``Vagrantfile``. There are
examples of common modifications commented in this repo's ``Vagrantfile``, or
you can learn more at https://www.vagrantup.com/

To customize the NixOS environment that will be provisioned, try modifying any
of the ``examples/<project>/configuration.nix`` files, or add your own project
in the ``examples`` folder and launch with:

```
NIXOS_PROJECT='yourproject' vagrant up
NIXOS_PROJECT='yourproject' vagrant ssh
```

You can learn more about NixOS from the manual: http://nixos.org/nixos/manual/


### Experiment

Now try modifying the `ghci` example by uncommenting the `tmux` package in
the `systemPackages` list.

You can use `vagrant` to automatically rebuild the `NixOS` configuration
with your changes:

```
NIXOS_PROJECT='ghci' vagrant provision
```

When you `ssh` in again (or even if you kept a prior `ssh` connection open), the
machine will now have `tmux` in addition to `ghci`.


### Why Vagrant?

The NixOS community already has a utility for automating deployments:
[NixOps](https://nixos.org/nixops/), so why use Vagrant?

Two reasons:

* NixOps is not trivial to install: you'll likely need to get a virtualbox VM with it pre-installed or install manually
* NixOps is not trivial to learn: it's a full-featured utility with a lot of options for customizing and creating deployments

The goal with this project is to make vagrant + virtualbox dependencies without
having to learn how to use them to get started. You can begin editing
`configuration.nix` files in the examples folder and launching VMs with them
almost immediately.


### VirtualBox VMs are slooooow

Compared to other container and virtualization tech, this process will be
somewhat slow. On the first provision it needs to download the vbox image,
and the first `vagrant up` can take a minute or two.

However, the beauty of NixOS is you don't actually need to provision a separate
VM for every role. This setup is to show off what you can do and to keep the
examples separate, but once you get going you can stick with one
`configuration.nix` file, and even after making extensive changes to it,
`vagrant provision` should go quickly (at that point the VM is running so it
only needs to run `nixos-rebuild switch` to rebuild using your config changes).


