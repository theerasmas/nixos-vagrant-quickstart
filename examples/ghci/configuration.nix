{ config, pkgs, ... }: with pkgs;

{

  environment.systemPackages = [
    ghc.ghc783
    # tmux
  ];

}
