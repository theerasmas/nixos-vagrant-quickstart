-- creates a database user for our snap application
CREATE USER snap WITH PASSWORD 'snappy';

CREATE DATABASE snap;

GRANT ALL PRIVILEGES ON DATABASE snap TO snap;
