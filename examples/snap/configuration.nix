# example stack that installs common dev tools, postgres, and the haskell
# snap framework.
# note that we expose port 8000 on the VM so you can serve snap applications
# on that port during development, and access them via your host machine

{ config, pkgs, ... }: with pkgs;

{

  # networking setup
  networking.firewall.allowedTCPPorts = [ 8000 ];

  # postgres setup
  services.postgresql = {
    enable = true;
    package = pkgs.postgresql93;
    # customize pg_hba.conf
    # allows you to run:
    #     psql -d snap -U snap -h 127.0.0.1
    # with password "snappy" (from the snap.sql file in this folder)
    authentication = pkgs.lib.mkOverride 10 ''
        local postgres root ident
        host snap snap 127.0.0.1/32 password
        local all all ident
    '';
    # vagrant will sync this file to the below location by default, but if you
    # alter the synced folders you'll need to move it or find another way to
    # place the file in the VM
    initialScript = "/vagrant/examples/snap/snap.sql";
  };

  # creates a system user that can be used to run your snap application
  users.extraUsers.snap = {
    name = "snap";
    group = "snap";
    createHome = true;
    home = "/home/snap";
    useDefaultShell = true;
    description = "Snap server system user";
  };

  # includes common development tools and can be modified to include anything
  # in nixpkgs
  environment.systemPackages =
  [ emacs24-nox
    vim
    git
    tmux
    ghc.ghc783
    haskellPackages_ghc783_no_profiling.snap
  ];


}
